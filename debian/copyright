Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kcalutils
Source: https://invent.kde.org/pim/kcalutils
Upstream-Contact: kde-devel@kde.org

Files: *
Copyright: 2017, Allen Winter <winter@kde.org>
           2001-2003, Cornelius Schumacher <schumacher@kde.org>
           2007, David Faure <faure@kde.org>
           2020, Glen Ditchfield <GJDitchfield@acm.org>
           2009-2010, Klarälvdalens Datakonsult AB, a KDAB Group company <info@kdab.net>
           Klarälvdalens Datakonsult AB, a KDAB Group company <info@kdab.net>
           1998, Preston Brown <pbrown@kde.org>
           2005, Rafal Rzepecki <divide@users.sourceforge.net>
           2003-2004, Reinhold Kainhofer <reinhold@kainhofer.com>
           2015, Sérgio Martins <iamsergio@gmail.com>
           2008, Thomas Thrainer <tom_t@gmx.at>
License: LGPL-2+

Files: src/config-kcalutils.h.in
       src/grantleeki18nlocalizer.cpp
       src/grantleeki18nlocalizer_p.h
       src/grantlee_plugin/datetimefilters.cpp
       src/grantlee_plugin/datetimefilters.h
       src/grantlee_plugin/icon.cpp
       src/grantlee_plugin/icon.h
       src/grantlee_plugin/kcalendargrantleeplugin.cpp
       src/grantlee_plugin/kcalendargrantleeplugin.h
       src/grantleetemplatemanager.cpp
       src/grantleetemplatemanager_p.h
       src/qtresourcetemplateloader.cpp
       src/qtresourcetemplateloader.h
Copyright: 2015, Daniel Vrátil <dvratil@redhat.com>
           2016-2024, Laurent Montel <montel@kde.org>
           2015, Volker Krause <vkrause@kde.org>
License: LGPL-2.1+

Files: autotests/CMakeLists.txt
       CMakeLists.txt
       CMakePresets.json
       KPimCalendarUtilsConfig.cmake.in
       src/CMakeLists.txt
       src/grantlee_plugin/CMakeLists.txt
Copyright: 2021-2024, Laurent Montel <montel@kde.org>
           none
License: BSD-3-Clause

Files: .git-blame-ignore-revs
       .gitignore
       .gitlab-ci.yml
       .kde-ci.yml
       .krazy
       Messages.sh
       metainfo.yaml
       README.md
       sanitizers.supp
Copyright: 2016, Daniel Vrátil <dvratil@kde.org>
           2020-2024, Laurent Montel <montel@kde.org>
           None
License: CC0-1.0

Files: po/ca/*
       po/ca@valencia/*
       po/uk/*
Copyright: 2014-2018, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV

Files: debian/*
Copyright: 2015 Harald Sitter <sitter@kde.org>
           2024 Patrick Franz <deltaone@debian.org>
License: LGPL-2+

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-2.1+3+KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN ATTORNEY-CLIENT
 RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS.
 CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE USE OF THIS DOCUMENT OR
 THE INFORMATION OR WORKS PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR
 DAMAGES RESULTING FROM THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
 PROVIDED HEREUNDER.
 .
 Statement of Purpose
 .
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator and
 subsequent owner(s) (each and all, an "owner") of an original work of
 authorship and/or a database (each, a "Work").
 .
 On Debian systems, the complete text of the Creative Commons Zero v1.0 Universal
 license can be found in "/usr/share/common-licenses/CC0-1.0".
